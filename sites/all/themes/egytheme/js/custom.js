jQuery(document).ready(function(){
    if ( jQuery('body').hasClass( "i18n-ar" ) ) {
        jQuery('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1200,
            arrows: false,
            dots: false,
            rtl: true,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    } else {
        jQuery('.customer-logos').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1200,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3
                }
            }]
        });
    }
  jQuery(function() {
    jQuery('.footer-links-holder h3').click(function () {
      jQuery(this).parent().toggleClass('active');
    });
  });
  if ( jQuery('body').hasClass( "i18n-ar" ) ) {
    jQuery(".en-footer").hide();
} else if ( jQuery('body').hasClass( "i18n-en" ) ) {
    jQuery(".ar-footer").hide();
}
});