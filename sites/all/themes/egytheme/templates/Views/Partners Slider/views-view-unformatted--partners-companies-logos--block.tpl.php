<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
global $base_url;
?>
<div class="container partners">
  <section class="customer-logos slider">
    <?php foreach ($rows as $id => $row): ?>
    <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] . '"'; } ?>>
      <div class="slide">
        <?php print $row; ?>
      </div>
</div>
<?php endforeach; ?>
</section>
</div>