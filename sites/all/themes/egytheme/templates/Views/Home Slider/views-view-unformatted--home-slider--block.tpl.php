<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
	    <?php $i = 1; ?>
	    <?php foreach ($rows as $id => $row): ?>
      <div class="item <?php $i == 1 ? print_r('active') : print_r('') ; ?>">
        <?php print $row; ?>		
        <?php $i++; ?>
      </div>
	    <?php endforeach; ?>
    </div>
    <ol class="carousel-indicators">
      <?php $j = 0; ?>
      <?php foreach ($rows as $id => $row): ?>
        <li data-target="#myCarousel" data-slide-to="<?php print_r($j); ?>" class="<?php $j == 0 ? print_r('active') : print_r('') ; ?>"></li>
        <?php $j++; ?> 
      <?php endforeach; ?>
    </ol>
</div>